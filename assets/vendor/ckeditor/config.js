/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.language = 'pt-br';
	//config.contentsCss = BASE+'/assets/vendor/ckeditor/estilos.css';
	//config.stylesSet = [];
	//config.extraPlugins = 'panel,floatpanel,richcombo,stylescombo,stylesheetparser';
	config.extraPlugins = 'panel,floatpanel,richcombo,stylescombo';
	config.stylesSet = 'custom_styles';
	
	config.toolbar = [
    	{ name: 'basicstyles', items: [ 'Bold', 'Italic' ] },
    	{ name: 'styles', items: [ 'Styles' ] },
		{ name: 'clipboard', items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
    	{ name: 'document', items: [ 'Source' ] },
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';
};
