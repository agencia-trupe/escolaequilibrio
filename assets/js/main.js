var mostrarCirculos = function(){
	var circulos = $('.circ.circescondido');
	if(circulos.length){
		var atual = $('.circ.circescondido').first();
		atual.removeClass('circescondido');
		setTimeout( function(){
		 	mostrarCirculos();
		}, 400);
	}
}

$('document').ready( function(){

	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: 'assets/js/polyfill-placeholder.js'
		}
	]);
	
	if($('.main-home').length){
		setTimeout( function(){
			mostrarCirculos();			
		}, 200)
	}

	$('.galeria a').fancybox({
		overlayColor : '#1D3153'
	});

	$('#form-contato').submit( function(e){

		if($('#contato-nome').val() == ''){
			alert('Informe seu Nome!');
			e.preventDefault();
			return false;
		}

		if($('#contato-email').val() == ''){
			alert('Informe seu e-mail!');
			e.preventDefault();
			return false;
		}

		if($('#contato-mensagem').val() == ''){
			alert('Informe sua mensagem!');
			e.preventDefault();
			return false;
		}
	});

	$('#cv-form').submit( function(e){

		if($('#trabalhe-nome').val() == ''){
			alert('Informe seu Nome!');
			e.preventDefault();
			return false;
		}

		if($('#trabalhe-email').val() == ''){
			alert('Informe seu e-mail!');
			e.preventDefault();
			return false;
		}

		if($('#trabalhe-arquivo').val() == ''){
			alert('Envie seu currículo!');
			e.preventDefault();
			return false;
		}
	});

	$('#cv-indique').submit( function(){
		
		if($('#indicar-nome_proprio').val() == ''){
			alert('Informe seu Nome!');
			e.preventDefault();
			return false;
		}

		if($('#indicar-email_proprio').val() == ''){
			alert('Informe seu E-mail!');
			e.preventDefault();
			return false;
		}

		if($('#indicar-nome_amigo').val() == ''){
			alert('Informe o nome da Pessoa que vai indicar!');
			e.preventDefault();
			return false;
		}

		if($('#indicar-email_amigo').val() == ''){
			alert('Informe o e-mail da Pessoa que vai indicar!');
			e.preventDefault();
			return false;
		}

		if($('#indicar-mensagem').val() == ''){
			alert('Informe uma mensagem para a Pessoa que vai indicar!');
			e.preventDefault();
			return false;
		}
		
	});

	$("#cv-form input[type='file']").change( function(){
		var path = $(this).val().split('\\');
		$(this).parent().find('.fake').html(path.slice('-1')[0]);
	});

});