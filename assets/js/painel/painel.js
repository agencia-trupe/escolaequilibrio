$('document').ready( function(){

  	$('.btn-delete').click( function(e){
    	e.preventDefault();
    	var form = $(this).closest('form');
    	bootbox.confirm("Deseja Excluir o Registro?", function(result){
	      	if(result)
	        	form.submit();
	      	else
	        	$(this).modal('hide');
    	});
  	});

    

    $("table.table-sortable tbody").sortable({
        update : function () {
            serial = [];
            tabela = $('table.table-sortable').attr('data-tabela');
            $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
                serial.push(elm.id.split('_')[1])
            });
            $.post('ajax/gravaOrdem', { data : serial , tabela : tabela });
        },
        helper: function(e, ui) {
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        },
        handle : $('.btn-move')
    }).disableSelection();



    if($('textarea').length){
        $('textarea').ckeditor();

        CKEDITOR.stylesSet.add( 'custom_styles', [
            // Block-level styles
            { name: 'Paragrafo Branco', element: 'p', styles: { 'margin' : '0', 'color' : '#fff', 'font-weight' : 'normal', 'font-family' : "'Varela Round', sans-serif", 'font-size' : '15px', 'line-height' : '130%' } },
            { name: 'Paragrafo Azul', element: 'p', styles: { 'margin' : '0', 'color' : '#61CCFF', 'font-weight' : 'normal', 'font-family' : "'Varela Round', sans-serif", 'font-size' : '15px', 'line-height' : '130%' } },
            { name: 'Paragrafo Azul Escuro', element: 'p', styles: { 'margin' : '0', 'color' : '#1D3153', 'font-weight' : 'normal', 'font-family' : "'Varela Round', sans-serif", 'font-size' : '15px', 'line-height' : '130%' } },
            { name: 'Paragrafo Destaque', element: 'p', styles: { 'margin' : '0', 'color' : '#61CCFF', 'font-weight' : 'normal', 'font-family' : "'Varela Round', sans-serif", 'font-size' : '22px', 'line-height' : '150%' } },
            { name: 'Lista Destaque', element: 'ul', styles: { 'margin' : '0', 'color' : '#61CCFF', 'font-weight' : 'normal', 'font-family' : "'Varela Round', sans-serif", 'font-size' : '22px', 'line-height' : '120%' } },
            { name: 'Assinatura', element : 'p', styles : { 'margin' : '0', 'color' : '#61CCFF', 'font-weight' : 'normal', 'font-family' : "'Lobster Two', cursive", 'font-size' : '27px', 'line-height' : '130%' }  },
            // Inline styles
            { name: 'Texto Branco', element: 'span', styles: { 'color': '#fff !important' } },
            { name: 'Texto Azul', element: 'span', styles: { 'color': '#61CCFF !important' } },
            { name: 'Texto Azul Escuro', element: 'span', styles: { 'color': '#1D3153 !important' } },
            { name: 'Texto Destaque', element: 'span', styles: { 'color': '#61CCFF !important', 'font-size' : '22px', 'line-height' : '150%' } },
        ]);
    }
    	
});	