-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: 21-Mar-2019 às 21:02
-- Versão do servidor: 5.7.22
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `escolaequilibrio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adwords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_pixel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `adwords`, `facebook_pixel`, `created_at`, `updated_at`) VALUES
(1, 'Escola Equilíbrio', 'Escola de educação infantil no Morumbi - desde 1984 - tradição, pioneirismo e qualidade em educação! Tel. (11) 3742-0521', 'escola, educação, infantil, ensino, qualidade, Morumbi, colégio, escola particular, pesquisa, atualidade, melhor, muito boa, crianças pequenas, meio período', '', 'UA-49155419-1', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `seo`
--

CREATE TABLE `seo` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pagina` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `seo`
--

INSERT INTO `seo` (`id`, `slug`, `pagina`, `title`, `description`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'home', 'Home', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'a-escola', 'A Escola', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'proposta-pedagogica', 'Proposta Pedagógica', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'estrutura', 'Estrutura', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'relatorios-e-resultados', 'Relatórios e Resultados', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'depoimentos', 'Depoimentos', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'atendimento', 'Atendimento', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'aulas-especificas', 'Aulas Específicas', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'contato', 'Contato', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'carta-fundadores', 'Carta dos Fundadores', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
