module.exports = function(grunt) {

  // Project configuration.
  	grunt.initConfig({
	  	modernizr: {

	    	dist: {
	        	// [REQUIRED] Path to the build you're using for development.
	        	"devFile" : "../assets/js/modernizr-dev.js",

	        	// [REQUIRED] Path to save out the built file.
	        	"outputFile" : "../assets/js/modernizr.js",

	        	// Based on default settings on http://modernizr.com/download/
	        	"extra" : {
	            	"shiv" : true,
	            	"printshiv" : false,
	            	"load" : true,
	            	"mq" : false,
	            	"cssclasses" : true
	        	},

	        	"tests" : ['input'],
	    	}
	    }
  	});

  // Load the plugin that provides the "uglify" task.
  //grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-modernizr');

  // Default task(s).
  //grunt.registerTask('default', ['']);
  //grunt.registerTask('default', ['modernizr']);

};