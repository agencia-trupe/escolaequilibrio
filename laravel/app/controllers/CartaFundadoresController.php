<?php

use \CartaFundadores;

class CartaFundadoresController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('contato_rodape', Contato::first());
		$this->layout->with('como_escolher', ComoEscolher::first());

		$this->layout->content = View::make('frontend.cartafundadores')->with('texto', CartaFundadores::first());
	}

}