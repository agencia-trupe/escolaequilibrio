<?php

use \RelatoriosEResultados;

class RelatoriosEResultadosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('contato_rodape', Contato::first());
		$this->layout->with('como_escolher', ComoEscolher::first());

		$this->layout->content = View::make('frontend.relatorioseresultados')->with('texto', RelatoriosEResultados::first());
	}

}