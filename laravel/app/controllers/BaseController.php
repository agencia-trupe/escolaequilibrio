<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function __construct()
	{
		$config = \Configuracoes::first() ?: null;
		$route  = \Route::currentRouteName();

		$title       = 'Escola Equilíbrio';
		$description = 'Escola de educação infantil no Morumbi - desde 1984 - tradição, pioneirismo e qualidade em educação! Tel. (11) 3742-0521';
		$keywords    = 'escola, educação, infantil, ensino, qualidade, Morumbi, colégio, escola particular, pesquisa, atualidade, melhor, muito boa, crianças pequenas, meio período';
		$shareImage  = 'http://escolaequilibrio.com.br/assets/images/layout/logo-equilibrio.png';
		$analytics   = 'UA-49155419-1';
		$adwords     = '';
		$fbPixel     = '';

		if ($config) {
			$title       = $config->title ?: $title;
			$description = $config->description ?: $description;
			$keywords    = $config->keywords ?: $keywords;
			$shareImage  = $config->imagem_de_compartilhamento
				? asset('assets/images/share/'.$config->imagem_de_compartilhamento)
				: $shareImage;
			$analytics   = $config->analytics;
			$adwords     = $config->adwords;
			$fbPixel     = $config->facebook_pixel;
		}

		if ($seoPage = \SEO::where('slug', $route)->first()) {
			$title       = $seoPage->title ?: $config->title;
			$description = $seoPage->description ?: $config->description;
			$keywords    = $seoPage->keywords ?: $config->keywords;
		}

		\View::share('title', $title);
		\View::share('description', $description);
		\View::share('keywords', $keywords);
		\View::share('shareImage', $shareImage);
		\View::share('analytics', $analytics);
		\View::share('adwords', $adwords);
		\View::share('fbPixel', $fbPixel);
	}

}
