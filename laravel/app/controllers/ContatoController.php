<?php

use \Contato, \Mail, \Request, \View;

class ContatoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('contato_rodape', Contato::first());
		$this->layout->with('como_escolher', ComoEscolher::first());

		$this->layout->content = View::make('frontend.contato')->with('texto', Contato::first());
	}

	public function enviar()
	{
		$data['nome'] = Request::get('nome');
		$data['email'] = Request::get('email');
		$data['telefone'] = Request::get('telefone');
		$data['mensagem'] = Request::get('mensagem');

		if($data['nome'] && $data['email'] && $data['mensagem']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
		    $message->to('felipe@escolaequilibrio.com.br', 'Escola Equilíbrio')
		    		->subject('Contato via site')
						->bcc('bruno@trupe.net')
						->bcc('fernando@trupe.net')
		    		->replyTo($data['email'], $data['nome']);
			});
		}

		Session::flash('envio', true);

		return Redirect::to('contato');
	}

	public function trabalhar()
	{
		$data['nome'] = Request::get('nome');
		$data['email'] = Request::get('email');
		$data['telefone'] = Request::get('telefone');

		if(Input::hasFile('arquivo')){

			$imagem = Input::file('arquivo');

			$random = '';
			do
			{
		    $filename = Str::slug(str_replace($imagem->getClientOriginalExtension(), '', $imagem->getClientOriginalName()).'_'.$random).'.'.$imagem->getClientOriginalExtension();
		    $file_path = 'assets/files/cvs/'.$filename;
		    $random = Str::random(6);
			}
			while (File::exists($file_path));
			$data['file_path'] = $file_path;
			$data['curriculo'] = $filename;

			$imagem->move('assets/files/cvs/', $filename);

			if($data['nome'] && $data['email'] && $data['curriculo']){
				Mail::send('emails.trabalhe', $data, function($message) use ($data)
				{
				    $message->to('felipe@escolaequilibrio.com.br', 'Escola Equilíbrio')
				    		->subject('Contato via site - Trabalhe Conosco')
								->bcc('bruno@trupe.net')
								->bcc('fernando@trupe.net')
				    		->attach($data['file_path'])
				    		->replyTo($data['email'], $data['nome']);
				});
			}

			return Redirect::to('contato#shadow-envie-curriculo-resposta');

		}else{
			Session::flash('erro', 'Houve um erro ao enviar o currículo. Tente novamente.');
			return Redirect::to('contato#shadow-envie-curriculo');
		}

	}

	public function indicar()
	{
		$data['nome_proprio'] = Request::get('nome_proprio');
		$data['email_proprio'] = Request::get('email_proprio');
		$data['nome_amigo'] = Request::get('nome_amigo');
		$data['email_amigo'] = Request::get('email_amigo');
		$data['mensagem'] = Request::get('mensagem');
		$backto = Request::get('backto');

		if($data['nome_proprio'] && $data['email_proprio'] && $data['nome_amigo'] && $data['email_amigo'] && $data['mensagem']){
			Mail::send('emails.indique', $data, function($message) use ($data)
			{
			    $message->to($data['email_amigo'], $data['nome_amigo'])
			    		->bcc('felipe@escolaequilibrio.com.br', 'Escola Equilíbrio')
			    		->subject('Indicação de '.$data['nome_proprio'])
			    		->replyTo($data['email_proprio'], $data['nome_proprio']);
			});
		}

		Session::flash('envio', true);

		return Redirect::to($backto.'#shadow-indicacao-resposta');
	}

}
