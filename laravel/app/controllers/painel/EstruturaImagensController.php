<?php

namespace Painel;

use \View, \Input, \Str, \Session, \File, \Image, \Redirect, \Hash, \EstruturaImagens;

class EstruturaImagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.estruturaimagens.index')->with('registros', EstruturaImagens::orderBy('ordem', 'asc')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.estruturaimagens.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new EstruturaImagens;

		$object->legenda = Input::get('legenda');

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');

			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/estrutura/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			
			$object->imagem = $filename;

			$imagem->move('assets/images/estrutura/', $filename);

			ini_set('memory_limit','256M');

			Image::make($file_path)->resize(295, 195, true, false)->save('assets/images/estrutura/thumbs/'.$filename, 100);
		}else{
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao Inserir Imagem!'));
		}
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem criada com sucesso.');
			return Redirect::route('painel.estrutura-imagens.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao Inserir Imagem!.'.$e->getMessage()));	

		}
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.estruturaimagens.edit')->with('registro', EstruturaImagens::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = EstruturaImagens::find($id);

		$object->legenda = Input::get('legenda');

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');

			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/estrutura/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			
			$object->imagem = $filename;

			$imagem->move('assets/images/estrutura/', $filename);

			ini_set('memory_limit','256M');

			Image::make($file_path)->resize(295, 195, true, false)->save('assets/images/estrutura/thumbs/'.$filename, 100);
		}
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Imagem alterada com sucesso.');
			return Redirect::route('painel.estrutura-imagens.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao Alterar Imagem!.'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = EstruturaImagens::find($id);
		@unlink('assets/images/estrutura/'.$object->imagem);
		@unlink('assets/images/estrutura/thumbs/'.$object->imagem);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Imagem removida com sucesso.');

		return Redirect::route('painel.estrutura-imagens.index');
	}

}