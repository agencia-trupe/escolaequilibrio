<?php

namespace Painel;

class SEOController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public function index()
	{
		$this->layout->content = \View::make('backend.seo.index')
            ->with('registros', \SEO::get());
	}

	public function edit($id)
	{
		$this->layout->content = \View::make('backend.seo.edit')->with('registro', \SEO::findOrFail($id));
	}

	public function update($id)
	{
		$registro = \SEO::findOrFail($id);

		$registro->title          = \Input::get('title');
		$registro->description    = \Input::get('description');
		$registro->keywords       = \Input::get('keywords');

		try {

			$registro->save();
			\Session::flash('sucesso', true);
			\Session::flash('mensagem', 'Página alterada com sucesso.');
			return \Redirect::route('painel.seo.index');

		} catch (\Exception $e) {

			\Session::flash('formulario', Input::all());
			return \Redirect::back()->withErrors(array('Erro ao alterar página!'));

		}
	}
}
