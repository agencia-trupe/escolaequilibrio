<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Configuracoes, \Validator, \File;

class ConfiguracoesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public function index()
	{
        $this->layout->content = View::make('backend.configuracoes.index')
            ->with('registro', Configuracoes::firstOrFail());
	}

	public function update($id)
	{
        $configuracoes = Configuracoes::findOrFail($id);

        $validator = Validator::make(array(
            'title'                      => Input::get('title'),
            'imagem_de_compartilhamento' => Input::file('imagem_de_compartilhamento')
        ), array(
            'title'                      => 'required',
            'imagem_de_compartilhamento' => 'image'
        ));

        if ($validator->fails())
        {
            Session::flash('formulario', Input::except('imagem_de_compartilhamento'));
			return Redirect::back()->withErrors($validator->messages());
		}

		$configuracoes->title          = Input::get('title');
		$configuracoes->description    = Input::get('description');
		$configuracoes->keywords       = Input::get('keywords');
		$configuracoes->analytics      = Input::get('analytics');
		$configuracoes->adwords        = Input::get('adwords');
		$configuracoes->facebook_pixel = Input::get('facebook_pixel');

		if(Input::hasFile('imagem_de_compartilhamento')){
			try {
				$imagem = Input::file('imagem_de_compartilhamento');
				$random = '';
				do {
					$filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
					$file_path = 'assets/images/share/'.$filename;
					$random = Str::random(6);
				} while (File::exists($file_path));

				$configuracoes->imagem_de_compartilhamento = $filename;
				$imagem->move('assets/images/share/', $filename);
			} catch(\Exception $e) {
				Session::flash('formulario', Input::all());
				return Redirect::back()->withErrors(array('Erro ao Inserir Imagem!'));
			}
		}

		try {

			$configuracoes->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Configurações alteradas com sucesso.');
			return Redirect::route('painel.configuracoes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao alterar configurações!'));

		}
	}
}
