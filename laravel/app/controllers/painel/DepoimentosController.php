<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Image, \File, \Hash, \Depoimentos;

class DepoimentosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.depoimentos.index')->with('registros', Depoimentos::orderBy('ordem', 'asc')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.depoimentos.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Depoimentos;

		$object->legenda = Input::get('legenda');

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');

			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/depoimentos/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			
			$object->imagem = $filename;

			$imagem->move('assets/images/depoimentos/', $filename);

			ini_set('memory_limit','256M');

			Image::make($file_path)->resize(400, null, true, false)->save('assets/images/depoimentos/thumbs/'.$filename, 100);
		}else{
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao Inserir Imagem!'));
		}
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Depoimentos criado com sucesso.');
			return Redirect::route('painel.depoimentos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao Inserir Depoimento!.'.$e->getMessage()));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.depoimentos.edit')->with('registro', Depoimentos::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Depoimentos::find($id);

		$object->legenda = Input::get('legenda');

		if(Input::hasFile('imagem')){

			$imagem = Input::file('imagem');

			$random = '';
			do
			{
			    $filename = Str::slug($imagem->getClientOriginalName()).'_'.$random.'.'.File::extension($imagem->getClientOriginalName());
			    $file_path = 'assets/images/depoimentos/'.$filename;
			    $random = Str::random(6);
			}
			while (File::exists($file_path));
			
			$object->imagem = $filename;

			$imagem->move('assets/images/depoimentos/', $filename);

			ini_set('memory_limit','256M');

			Image::make($file_path)->resize(400, null, true, false)->save('assets/images/depoimentos/thumbs/'.$filename, 100);
		}
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Depoimento alterado com sucesso.');
			return Redirect::route('painel.depoimentos.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao Alterar Depoimento!.'));	

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Depoimentos::find($id);
		@unlink('assets/images/depoimentos/'.$object->imagem);
		@unlink('assets/images/depoimentos/thumbs/'.$object->imagem);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Depoimento removido com sucesso.');

		return Redirect::route('painel.depoimentos.index');
	}

}