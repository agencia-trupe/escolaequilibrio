<?php

use \PropostaPedagogica;

class PropostaPedagogicaController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('contato_rodape', Contato::first());
		$this->layout->with('como_escolher', ComoEscolher::first());

		$this->layout->content = View::make('frontend.propostapedagogica')->with('texto', PropostaPedagogica::first());
	}

}