<?php

use \Estrutura, \EstruturaImagens;

class EstruturaController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->with('contato_rodape', Contato::first());
		$this->layout->with('como_escolher', ComoEscolher::first());

		$this->layout->content = View::make('frontend.estrutura')->with('texto', Estrutura::first())
																 ->with('imagens', EstruturaImagens::orderBy('ordem', 'asc')->get());
	}

}