<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contato', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('telefone');
			$table->text('endereco');
			$table->text('endereco_rodape');
			$table->text('endereco_detalhes');
			$table->text('google_maps');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contato', function(Blueprint $table)
		{
			Schema::drop('contato');
		});
	}

}