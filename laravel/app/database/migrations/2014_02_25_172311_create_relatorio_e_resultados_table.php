<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelatorioEResultadosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('relatorios_e_resultados', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('texto_relatorio');
			$table->text('texto_resultados');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('relatorios_e_resultados', function(Blueprint $table)
		{
			Schema::drop('relatorios_e_resultados');
		});
	}

}