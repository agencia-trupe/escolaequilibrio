<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropostaPedagogicaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proposta_pedagogica', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('texto');
			$table->text('destaque1');
			$table->text('destaque2');
			$table->text('destaque3');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('proposta_pedagogica', function(Blueprint $table)
		{
			Schema::drop('proposta_pedagogica');
		});
	}

}