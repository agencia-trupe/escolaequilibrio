<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartaFundadoresTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('carta_fundadores', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('texto1');
			$table->text('texto2');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('carta_fundadores', function(Blueprint $table)
		{
			Schema::drop('carta_fundadores');
		});
	}

}