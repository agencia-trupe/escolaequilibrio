<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracoesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('configuracoes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
            $table->text('description');
            $table->text('keywords');
            $table->string('imagem_de_compartilhamento');
            $table->string('analytics');
            $table->string('adwords');
            $table->string('facebook_pixel');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('configuracoes');
	}

}
