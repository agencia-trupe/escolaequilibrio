<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepoimentosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('depoimentos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('imagem');
			$table->text('legenda')->nullable();
			$table->integer('ordem')->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('depoimentos', function(Blueprint $table)
		{
			Schema::drop('depoimentos');
		});
	}

}