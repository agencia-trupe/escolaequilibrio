<?php

class SEOSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
                'slug'        => 'home',
                'pagina'      => 'Home',
                'title'       => '',
                'description' => '',
                'keywords'    => '',
            ],
            [
                'slug'        => 'a-escola',
                'pagina'      => 'A Escola',
                'title'       => '',
                'description' => '',
                'keywords'    => '',
            ],
            [
                'slug'        => 'proposta-pedagogica',
                'pagina'      => 'Proposta Pedagógica',
                'title'       => '',
                'description' => '',
                'keywords'    => '',
            ],
            [
                'slug'        => 'estrutura',
                'pagina'      => 'Estrutura',
                'title'       => '',
                'description' => '',
                'keywords'    => '',
            ],
            [
                'slug'        => 'relatorios-e-resultados',
                'pagina'      => 'Relatórios e Resultados',
                'title'       => '',
                'description' => '',
                'keywords'    => '',
            ],
            [
                'slug'        => 'depoimentos',
                'pagina'      => 'Depoimentos',
                'title'       => '',
                'description' => '',
                'keywords'    => '',
            ],
            [
                'slug'        => 'atendimento',
                'pagina'      => 'Atendimento',
                'title'       => '',
                'description' => '',
                'keywords'    => '',
            ],
            [
                'slug'        => 'aulas-especificas',
                'pagina'      => 'Aulas Específicas',
                'title'       => '',
                'description' => '',
                'keywords'    => '',
            ],
            [
                'slug'        => 'contato',
                'pagina'      => 'Contato',
                'title'       => '',
                'description' => '',
                'keywords'    => '',
            ],
            [
                'slug'        => 'carta-fundadores',
                'pagina'      => 'Carta dos Fundadores',
                'title'       => '',
                'description' => '',
                'keywords'    => '',
            ],
        ];

        DB::table('seo')->delete();
        DB::table('seo')->insert($data);
    }

}


