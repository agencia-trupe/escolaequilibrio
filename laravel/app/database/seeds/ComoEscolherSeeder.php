<?php

class ComoEscolherSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'texto' => '<p>Texto</p>'
            ]
        ];

        DB::table('como_escolher')->delete();
        DB::table('como_escolher')->insert($data);
    }

}