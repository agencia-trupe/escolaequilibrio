<?php

class RelatoriosEResultadosSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'texto_relatorio' => '<p>Texto Relatórios</p>',
            	'texto_resultados' => '<p>Texto Resultados</p>',
            ]
        ];

        DB::table('relatorios_e_resultados')->delete();
        DB::table('relatorios_e_resultados')->insert($data);
    }

}