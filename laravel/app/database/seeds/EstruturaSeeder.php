<?php

class EstruturaSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'texto' => '<p>Texto</p>',
            ]
        ];

        DB::table('estrutura')->delete();
        DB::table('estrutura')->insert($data);
    }

}