<?php

class AtendimentoSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'texto' => '<p>Texto</p>',
            ]
        ];
        
        DB::table('atendimento')->delete();
        DB::table('atendimento')->insert($data);
    }

}