<?php

class UsuariosSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'email' => 'contato@trupe.net',
				'username' => 'trupe',
				'password' => Hash::make('senhatrupe')
            ]
        ];

		DB::table('usuarios')->delete();
        DB::table('usuarios')->insert($data);
    }

}