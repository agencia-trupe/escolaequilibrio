<?php

class ConfiguracoesSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
                'title'                      => 'Escola Equilíbrio',
                'description'                => 'Escola de educação infantil no Morumbi - desde 1984 - tradição, pioneirismo e qualidade em educação! Tel. (11) 3742-0521',
                'keywords'                   => 'escola, educação, infantil, ensino, qualidade, Morumbi, colégio, escola particular, pesquisa, atualidade, melhor, muito boa, crianças pequenas, meio período',
                'imagem_de_compartilhamento' => '',
                'analytics'                  => 'UA-49155419-1',
                'adwords'                    => '',
                'facebook_pixel'             => '',
            ]
        ];

        DB::table('configuracoes')->delete();
        DB::table('configuracoes')->insert($data);
    }

}
