<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'telefone' => '(11) 3742-0521',
            	'endereco' => '<p>Rua Professor Hilário Veiga de Carvalho, 60<br>Morumbi - São Paulo - Capital</p>',
            	'endereco_rodape' => 'Rua Professor Hilário Veiga de Carvalho, 60 &bull; Morumbi &bull; São Paulo - Capital',
            	'endereco_detalhes' => '(Em frente ao Portal do Morumbi / ao lado do Outback e Pão de Açucar)',
            	'google_maps' => "<iframe width='425' height='350' frameborder='0' scrolling='no' marginheight='0' marginwidth='0' src='https://maps.google.com.br/maps?q=Rua+Professor+Hil%C3%A1rio+Veiga+de+Carvalho,+60&amp;ie=UTF8&amp;hq=&amp;hnear=Rua+Professor+Hil%C3%A1rio+Veiga+de+Carvalho,+60+-+Vila+Suzana,+S%C3%A3o+Paulo,+05641-070&amp;gl=br&amp;t=m&amp;z=14&amp;ll=-23.619003,-46.737718&amp;output=embed'></iframe><br /><small><a href='https://maps.google.com.br/maps?q=Rua+Professor+Hil%C3%A1rio+Veiga+de+Carvalho,+60&amp;ie=UTF8&amp;hq=&amp;hnear=Rua+Professor+Hil%C3%A1rio+Veiga+de+Carvalho,+60+-+Vila+Suzana,+S%C3%A3o+Paulo,+05641-070&amp;gl=br&amp;t=m&amp;z=14&amp;ll=-23.619003,-46.737718&amp;source=embed' style='color:#0000FF;text-align:left'>Exibir mapa ampliado</a></small>",
            ]
        ];

        DB::table('contato')->delete();
        DB::table('contato')->insert($data);
    }

}