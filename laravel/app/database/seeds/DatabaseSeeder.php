<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('AEscolaSeeder');
		$this->call('AtendimentoSeeder');
		$this->call('AulasEspecificasSeeder');
		$this->call('CartaFundadoresSeeder');
		$this->call('ComoEscolherSeeder');
		$this->call('ContatoSeeder');
		$this->call('EstruturaSeeder');
		$this->call('PropostaPedagogicaSeeder');
		$this->call('RelatoriosEResultadosSeeder');
		$this->call('UsuariosSeeder');
		$this->call('ConfiguracoesSeeder');
		$this->call('SEOSeeder');
	}

}
