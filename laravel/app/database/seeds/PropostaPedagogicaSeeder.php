<?php

class PropostaPedagogicaSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'texto' => '<p>Texto</p>',
                'destaque1' => '<p>destaque 1</p>',
                'destaque2' => '<p>destaque 2</p>',
                'destaque3' => '<p>destaque 3</p>'
            ]
        ];

        DB::table('proposta_pedagogica')->delete();
        DB::table('proposta_pedagogica')->insert($data);
    }

}