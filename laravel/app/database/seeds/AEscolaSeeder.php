<?php

class AEscolaSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'texto' => '<p>Texto</p>'
            ]
        ];

        DB::table('a_escola')->delete();
        DB::table('a_escola')->insert($data);
    }

}