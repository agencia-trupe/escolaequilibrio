<?php

class AulasEspecificasSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'texto' => '<p>Texto</p>',
            ]
        ];

        DB::table('aulas_especificas')->delete();
        DB::table('aulas_especificas')->insert($data);
    }

}