<?php

class CartaFundadoresSeeder extends Seeder {

    public function run()
    {
        $data = [
            [
            	'texto1' => '<p>Texto 1</p>',
                'texto2' => '<p>Texto 2</p>'
            ]
        ];

        DB::table('carta_fundadores')->delete();
        DB::table('carta_fundadores')->insert($data);
    }

}