<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('a-escola', array('as' => 'a-escola', 'uses' => 'AEscolaController@index'));
Route::get('atendimento', array('as' => 'atendimento', 'uses' => 'AtendimentoController@index'));
Route::get('aulas-especificas', array('as' => 'aulas-especificas', 'uses' => 'AulasEspecificasController@index'));
Route::get('carta-fundadores', array('as' => 'carta-fundadores', 'uses' => 'CartaFundadoresController@index'));
Route::get('depoimentos', array('as' => 'depoimentos', 'uses' => 'DepoimentosController@index'));
Route::get('estrutura', array('as' => 'estrutura', 'uses' => 'EstruturaController@index'));
Route::get('proposta-pedagogica', array('as' => 'proposta-pedagogica', 'uses' => 'PropostaPedagogicaController@index'));
Route::get('relatorios-e-resultados', array('as' => 'relatorios-e-resultados', 'uses' => 'RelatoriosEResultadosController@index'));
Route::get('contato', array('as' => 'contato', 'uses' => 'ContatoController@index'));
Route::post('contato', array('as' => 'contato.enviar', 'uses' => 'ContatoController@enviar'));
Route::post('contato-trabalhar', array('as' => 'contato.trabalhar', 'uses' => 'ContatoController@trabalhar'));
Route::post('contato-indicar', array('as' => 'contato.indicar', 'uses' => 'ContatoController@indicar'));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('painel/ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
    Route::resource('a-escola', 'Painel\AEscolaController');
    Route::resource('atendimento', 'Painel\AtendimentoController');
    Route::resource('aulas-especificas', 'Painel\AulasEspecificasController');
    Route::resource('carta-fundadores', 'Painel\CartaFundadoresController');
    Route::resource('como-escolher', 'Painel\ComoEscolherController');
    Route::resource('contato', 'Painel\ContatoController');
    Route::resource('depoimentos', 'Painel\DepoimentosController');
    Route::resource('estrutura', 'Painel\EstruturaController');
    Route::resource('estrutura-imagens', 'Painel\EstruturaImagensController');
    Route::resource('proposta-pedagogica', 'Painel\PropostaPedagogicaController');
    Route::resource('relatorios-e-resultados', 'Painel\RelatoriosEResultadosController');
	Route::resource('usuarios', 'Painel\UsuariosController');
	Route::resource('configuracoes', 'Painel\ConfiguracoesController', array(
		'only' => array('index', 'update')
	));
	Route::resource('seo', 'Painel\SEOController', array(
		'only' => array('index', 'edit', 'update')
	));
});
