<?php

class SEO extends Eloquent
{
	protected $table = 'seo';

	protected $hidden = array();

    protected $fillable = array('', 'created_at', 'updated_at');

    protected $guarded = array('id');
}
