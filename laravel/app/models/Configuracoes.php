<?php

class Configuracoes extends Eloquent
{
	protected $table = 'configuracoes';

	protected $hidden = array();

    protected $fillable = array('', 'created_at', 'updated_at');

    protected $guarded = array('id');
}
