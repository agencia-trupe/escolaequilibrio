<!DOCTYPE html>
<html>
<head>
    <title>{{ $nome_proprio }} está indicando o site Escola Equilíbrio</title>
    <meta charset="utf-8">
</head>
<body>
	Olá {{$nome_amigo}},<br>
	{{ $nome_proprio }} lhe enviou a recomendação do site Escola Equilíbrio com a seguinte mensagem:<br>
	{{$mensagem}}<br>
    Saiba mais sobre a Escola Equilíbrio: <a href="http://www.escolaequilibrio.com.br" title="Escola Equilibrio">www.escolaequilibrio.com.br</a>
</body>
</html>