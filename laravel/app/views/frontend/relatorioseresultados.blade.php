@section('conteudo')

	<div class="colunas">
		<div class="coluna coluna-1-2">
			<div class="pad">
				<h1>Relatórios</h1>
				
				{{ $texto->texto_relatorio }}

				<img src="{{ asset('assets/images/layout/mao-tinta.png') }}" alt="Escola Equilíbrio" style="margin-top:15px;">
				
			</div>
		</div>
		<div class="coluna coluna-1-2">
			<div class="pad">
				<h1>Resultados</h1>

				{{ $texto->texto_resultados }}
			</div>
		</div>
	</div>

@stop