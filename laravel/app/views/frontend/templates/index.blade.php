<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="robots" content="index, follow" />

	<meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
	<meta name="description" content="{{ $description }}">
    <meta name="keywords" content="{{ $keywords }}" />

	<meta property="og:title" content="{{ $title }}"/>
	<meta property="og:description" content="{{ $description }}"/>
    <meta property="og:site_name" content="{{ $title }}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="{{ $shareImage }}"/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<title>{{ $title }}</title>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/fancybox/source/jquery.fancybox',
		'css/base',
		'css/'.str_replace('-', '', Route::currentRouteName())
	))?>

	@if(Route::currentRouteName() == 'consumidor.home')
		<?=Assets::CSS(array('consumidor/home'))?>
	@else
		<?=Assets::CSS(array('consumidor/internas'))?>
	@endif

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<?=Assets::CSS('css/responsive')?>

	<!--[if lt IE 10]>
		<?=Assets::CSS('css/iefix')?>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<?=Assets::JS(array('vendor/selectivizr/selectivizr'))?>
	<![endif]-->
	<!--[if gte IE 10]><!-->
    	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<!--<![endif]-->


	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('js/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('js/modernizr'))?>
	@endif
</head>
<body>

	<header>

		<div id="barra-azul"></div>

		<div class="centro">

			<a href="{{ Request::url() }}#shadow-indicacao" id="link-indicar" title="Indicar para um amigo"><img src="{{asset('assets/images/layout/icon-email.png')}}" alt="Enviar">Indicar para um amigo</a>

			<nav>
				<a href="{{ URL::route('home') }}" id="link-home">
					<img src="{{ asset('assets/images/layout/logo-equilibrio.png') }}" alt="Escola Equilíbrio">
				</a>
				<ul>
					<li><a href="{{ URL::route('home') }}" @if(Route::currentRouteName() == 'home') class="ativo" @endif title="Página Inicial">home</a></li>
					<li><a href="{{ URL::route('a-escola') }}" @if(Route::currentRouteName() == 'a-escola' || Route::currentRouteName() == 'carta-fundadores') class="ativo" @endif title="A Escola">a escola</a></li>
					<li><a href="{{ URL::route('proposta-pedagogica') }}" @if(Route::currentRouteName() == 'proposta-pedagogica') class="ativo" @endif title="Proposta Pedagógica">proposta pedagógica</a></li>
					<li><a href="{{ URL::route('estrutura') }}" @if(Route::currentRouteName() == 'estrutura') class="ativo" @endif title="Estrutura">estrutura</a></li>
					<li><a href="{{ URL::route('relatorios-e-resultados') }}" @if(Route::currentRouteName() == 'relatorios-e-resultados') class="ativo" @endif title="Relatórios e Resultados">relatórios e resultados</a></li>
					<li><a href="{{ URL::route('depoimentos') }}" @if(Route::currentRouteName() == 'depoimentos') class="ativo" @endif title="Depoimentos">depoimentos</a></li>
					<li><a href="{{ URL::route('atendimento') }}" @if(Route::currentRouteName() == 'atendimento') class="ativo" @endif title="Atendimento">atendimento</a></li>
					<li><a href="{{ URL::route('aulas-especificas') }}" @if(Route::currentRouteName() == 'aulas-especificas') class="ativo" @endif title="Aulas Específicas">aulas específicas</a></li>
					<li><a href="{{ URL::route('contato') }}" @if(Route::currentRouteName() == 'contato') class="ativo" @endif title="Contato">contato</a></li>
				</ul>
				<div id="frase">
					<div class="texto">
						Tradição, pioneirismo<br>
						e qualidade em educação.
					</div>
				</div>
				<button id="mobile-toggle" type="button" role="button">
                	<span class="lines"></span>
            	</button>
			</nav>
		</div>
	</header>

	<div id="nav-mobile">
		<div class="centro">
			<a href="{{ URL::route('home') }}" @if(Route::currentRouteName() == 'home') class="ativo" @endif title="Página Inicial">home</a>
			<a href="{{ URL::route('a-escola') }}" @if(Route::currentRouteName() == 'a-escola' || Route::currentRouteName() == 'carta-fundadores') class="ativo" @endif title="A Escola">a escola</a>
			<a href="{{ URL::route('proposta-pedagogica') }}" @if(Route::currentRouteName() == 'proposta-pedagogica') class="ativo" @endif title="Proposta Pedagógica">proposta pedagógica</a>
			<a href="{{ URL::route('estrutura') }}" @if(Route::currentRouteName() == 'estrutura') class="ativo" @endif title="Estrutura">estrutura</a>
			<a href="{{ URL::route('relatorios-e-resultados') }}" @if(Route::currentRouteName() == 'relatorios-e-resultados') class="ativo" @endif title="Relatórios e Resultados">relatórios e resultados</a>
			<a href="{{ URL::route('depoimentos') }}" @if(Route::currentRouteName() == 'depoimentos') class="ativo" @endif title="Depoimentos">depoimentos</a>
			<a href="{{ URL::route('atendimento') }}" @if(Route::currentRouteName() == 'atendimento') class="ativo" @endif title="Atendimento">atendimento</a>
			<a href="{{ URL::route('aulas-especificas') }}" @if(Route::currentRouteName() == 'aulas-especificas') class="ativo" @endif title="Aulas Específicas">aulas específicas</a>
			<a href="{{ URL::route('contato') }}" @if(Route::currentRouteName() == 'contato') class="ativo" @endif title="Contato">contato</a>
		</div>
	</div>

	<!-- Conteúdo Principal -->
	<div class="main {{ 'main-'. str_replace('-', '', Route::currentRouteName()) }}">
		@yield('conteudo')
	</div>

	<?=Assets::JS(array(
		'vendor/fancybox/source/jquery.fancybox',
		'js/main'
	))?>

	<footer>
		<div class="centro">
			<div id="aviso">Visando a privacidade de nossos alunos, este site não contém fotos dos mesmos.</div>
			<div class="endereco">
				<div class="endereco-rodape">{{$contato_rodape->endereco_rodape}}</div>
				<div class="detalhe-endereco">{{$contato_rodape->endereco_detalhes}}</div>
				<div class="telefone">{{$contato_rodape->telefone}}</div>
			</div>

			<div id="assinatura">
				&copy; <?=Date('Y')?> Escola Equilíbrio - Todos os direitos reservados.<br>
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites: Trupe Agência Criativa">Criação de Sites:</a> <a href="http://www.trupe.net" target="_blank" title="Criação de Sites: Trupe Agência Criativa">Trupe Agência Criativa</a>
			</div>

		</div>
	</footer>

	<div id="shadow-como-escolher">
		<div class="centro">
			<a href="#!" title="Fechar">FECHAR [X]</a>
			<div class="texto">
				<h1>Como escolher uma Escola para seu filho</h1>

				{{ $como_escolher->texto }}

			</div>
		</div>
	</div>

	<div id="shadow-envie-curriculo">
		<div class="centro">
			<a href="contato#!" title="Fechar">FECHAR [X]</a>
			<div class="texto">
				<h1>ENVIE SEU CURRÍCULO</h1>

				@if(Session::has('erro'))
					<div class="erro">
						<h1 style="color:red; font-size:16px;">{{ Session::get('erro') }}</h1>
					</div>
				@endif

				<form action="{{URL::route('contato.trabalhar')}}" method="post" id="cv-form" enctype="multipart/form-data">
					<input type="text" name="nome" placeholder="nome" required id="trabalhe-nome">
					<input type="email" name="email" placeholder="e-mail" required id="trabalhe-email">
					<input type="text" name="telefone" placeholder="telefone">
					<label class="file">
						<div class="fake">ANEXAR CURRÍCULO</div>
						<input type="file" name="arquivo" required id="trabalhe-arquivo">
					</label>
					<input type="submit" value="ENVIAR">
				</form>
			</div>
		</div>
	</div>

	<div id="shadow-envie-curriculo-resposta">
		<div class="centro">
			<a href="contato#!" title="Fechar">FECHAR [X]</a>
			<div class="texto">
				<h1>ENVIE SEU CURRÍCULO</h1>

				<h2>SEU	CURRÍCULO FOI ENVIADO</h2>

				<h2>AGRADECEMOS SEU INTERESSE.<br>RETORNAREMOS EM BREVE</h2>

				<a href="contato#!" id="fechar-grande" title="Fechar">FECHAR</a>
			</div>
		</div>
	</div>

	<div id="shadow-indicacao">
		<div class="centro">
			<a href="{{ Request::url() }}#!" title="Fechar">FECHAR [X]</a>
			<div class="texto">
				<h1>INDICAR PARA UM AMIGO</h1>

				<form action="{{URL::route('contato.indicar')}}" method="post" id="cv-indique">
					<input type="text" name="nome_proprio" placeholder="seu nome" required id="indicar-nome_proprio">
					<input type="email" name="email_proprio" placeholder="seu e-mail" required id="indicar-email_proprio">
					<input type="text" name="nome_amigo" placeholder="nome do seu amigo" required id="indicar-nome_amigo">
					<input type="email" name="email_amigo" placeholder="e-mail do seu amigo" required id="indicar-email_amigo">
					<textarea name="mensagem" placeholder="mensagem" required id="indicar-mensagem"></textarea>
					<input type="hidden" name="backto" value="{{ Request::url() }}">
					<input type="submit" value="ENVIAR">
				</form>
			</div>
		</div>
	</div>

	<div id="shadow-indicacao-resposta">
		<div class="centro">
			<a href="{{ Request::url() }}#!" title="Fechar">FECHAR [X]</a>
			<div class="texto">
				<h1>INDICAR PARA UM AMIGO</h1>

				<h2>SUA	INDICAÇÃO FOI ENVIADA</h2>
				<h2>OBRIGADO!</h2>

				<a href="{{ Request::url() }}#!" id="fechar-grande" title="Fechar">FECHAR</a>
			</div>
		</div>
	</div>

	<script>
		$('#mobile-toggle').on('click touchstart', function(event) {
			event.preventDefault();
			$('#nav-mobile').slideToggle();
			$('#mobile-toggle').toggleClass('close');
		});
	</script>

@if($analytics)
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
		Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', {{ $analytics }}, 'escolaequilibrio.com.br');
		ga('send', 'pageview');
	</script>
@endif

@if($fbPixel)
	<!-- Facebook Pixel Code -->
	<script>
	!function(f,b,e,v,n,t,s)
	{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];
	s.parentNode.insertBefore(t,s)}(window, document,'script',
	'https://connect.facebook.net/en_US/fbevents.js');
	fbq('init', '{{ $fbPixel }}');
	fbq('track', 'PageView');
	</script>
	<noscript>
	<img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id={{ $fbPixel }}&ev=PageView&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
@endif

@if($adwords)
	<!-- Global Site Tag (gtag.js) - Google AdWords: GOOGLE_CONVERSION_ID -->
	<script async src="https://www.googletagmanager.com/gtag/js?id={{ $adwords }}"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', '{{ $adwords }}');
	</script>
@endif
</body>
</html>
