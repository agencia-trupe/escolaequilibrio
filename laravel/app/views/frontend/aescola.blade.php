@section('conteudo')

	<div class="colunas">
		<div class="coluna coluna-1-2">
			<div class="pad">
				<h1>Histórico</h1>
				
				{{ $texto->texto }}

				<div class="chamada-redonda">
					<a href="{{ URL::route('carta-fundadores') }}" title="Carta dos Fundadores">
						Carta dos Fundadores
						<span>clique aqui para visualizar</span>
					</a>
				</div>
			</div>
		</div>
		<div class="coluna coluna-1-2">
			<div class="pad">
				<img src="{{ asset('assets/images/layout/menina-coracao.png') }}" alt="Escola Equilíbrio" style="margin-left:85px;">
				<img src="{{ asset('assets/images/layout/figura-pag.png') }}" alt="Escola Equilíbrio">
			</div>
		</div>
	</div>

@stop