@section('conteudo')

	<div class="bola-grande circ circescondido">
		<img src="{{ asset('assets/images/layout/bola1-grande.png') }}" alt="Escola Equilíbrio">
	</div>

	<div class="bola-pequena bola1 circ circescondido">
		<img src="{{ asset('assets/images/layout/bola2.png') }}" alt="Escola Equilíbrio">
	</div>
	<div class="bola-pequena bola2 circ circescondido">
		<img src="{{ asset('assets/images/layout/bola3.png') }}" alt="Escola Equilíbrio">
	</div>
	<div class="bola-pequena bola3 circ circescondido">
		<img src="{{ asset('assets/images/layout/bola4.png') }}" alt="Escola Equilíbrio">
	</div>
	<div class="bola-pequena bola4 circ circescondido">
		<img src="{{ asset('assets/images/layout/bola5.png') }}" alt="Escola Equilíbrio">
	</div>
	
	<div class="chamadas">
		<a href="{{ URL::route('carta-fundadores') }}" title="Carta dos Fundadores" class="chamada escura">
			Carta dos<br> Fundadores
		</a>
		<a href="#shadow-como-escolher" title="Saiba como escolher uma escola para o seu filho" class="chamada clara">
			Saiba como<br> escolher uma<br> escola para o<br> seu filho
		</a>
		<a href="{{ URL::route('contato') }}" title="Nossa localização. Como chegar." class="chamada escura">
			Nossa<br> localização.<br> Como chegar.
		</a>
	</div>

@stop