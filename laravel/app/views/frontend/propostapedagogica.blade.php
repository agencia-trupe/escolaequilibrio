@section('conteudo')

	<div class="colunas">
		<div class="coluna coluna-1-2">
			<div class="pad">
				<h1>Proposta Pedagógica</h1>
				
				{{ $texto->texto }}
				
				<img src="{{ asset('assets/images/layout/proposta-pedagogica.png') }}" alt="Escola Equilíbrio" style="margin-top:15px;">
			</div>
		</div>
		<div class="coluna coluna-1-2">
			<div class="pad">
				<div class="texto-redondo IEprimeiro">{{$texto->destaque1}}</div>
				<div class="texto-redondo IEsegundo">{{$texto->destaque2}}</div>
				<div class="texto-redondo IEterceiro">{{$texto->destaque3}}</div>
			</div>
		</div>
	</div>

@stop