@section('conteudo')

	<div class="colunas">
		<div class="coluna coluna-1-2">
			<div class="pad">
				<h1>Carta dos Fundadores</h1>
				
				{{ $texto->texto1 }}
			</div>
		</div>
		<div class="coluna coluna-1-2">
			<div class="pad">
				<img src="{{ asset('assets/images/layout/menino-torre.png') }}" alt="Escola Equilíbrio" style="margin-left:138px; margin-bottom:20px;">
				{{ $texto->texto2 }}
			</div>
		</div>
	</div>

@stop