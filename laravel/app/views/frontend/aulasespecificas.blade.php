@section('conteudo')

	<div class="colunas">
		<div class="coluna coluna-1-2">
			<div class="pad">
				<h1>Aulas Específicas</h1>
				
				{{ $texto->texto }}
				
			</div>
		</div>
		<div class="coluna coluna-1-2">
			<div class="pad">
				<img src="{{ asset('assets/images/layout/aulas-especificas.png') }}" alt="Escola Equilíbrio" style="margin-left:0px;">				
			</div>
		</div>
	</div>

@stop