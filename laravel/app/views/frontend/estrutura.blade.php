@section('conteudo')

	<div class="colunas">
		<div class="coluna coluna-1-2">
			<div class="pad">
				<h1>Espaço Físico</h1>
				
				{{ $texto->texto }}
				
			</div>
		</div>
		<div class="coluna coluna-1-2">
			<div class="pad">
				<img src="{{ asset('assets/images/layout/espaco-fisico.png') }}" alt="Escola Equilíbrio" style="margin-left:0px;">
			</div>
		</div>
		<div class="coluna coluna-1 galeria">
			@if($imagens)
				@foreach ($imagens as $key => $value)
					<a href="{{asset('assets/images/estrutura/'.$value->imagem)}}" rel="galeria" title="{{$value->legenda}}">
						<img src="{{asset('assets/images/estrutura/thumbs/'.$value->imagem)}}" alt="Escola Equilíbrio">
					</a>
				@endforeach
			@endif
		</div>
	</div>

@stop