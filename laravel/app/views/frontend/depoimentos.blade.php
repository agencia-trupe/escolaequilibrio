@section('conteudo')

	<div class="colunas">
		<div class="coluna coluna-1">
			<div class="pad">
				<h1>Depoimentos de Ex-alunos</h1>
				
				@if($depoimentos)
					@foreach($depoimentos as $k => $v)
						<div class="depo">
							<img src="{{asset('assets/images/depoimentos/thumbs/'.$v->imagem)}}" alt="{{$v->legenda}}">
							<div class="legenda">{{$v->legenda}}</div>
						</div>
					@endforeach					
				@endif
			</div>
		</div>		
	</div>

@stop