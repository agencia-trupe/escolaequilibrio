@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if($errors->any())
		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	@endif

	<h2>
  		A Escola
	</h2>

  	<table class="table table-striped table-bordered table-hover">

  		<thead>
    		<tr>
                <th>Texto</th>
          		<th><span class="glyphicon glyphicon-cog"></span></th>
        	</tr>
  		</thead>

  		<tbody>
    	@foreach ($registro as $r)

        	<tr class="tr-row" id="row_{{ $r->id }}">
                <td>{{ Str::words(strip_tags($r->texto), 50) }}</td>
                <td class="crud-actions">
            		<a href="{{ URL::route('painel.a-escola.edit', $r->id ) }}" class="btn btn-primary btn-sm">editar</a>
          		</td>
        	</tr>

    	@endforeach
  		</tbody>

	</table>

</div>

@stop