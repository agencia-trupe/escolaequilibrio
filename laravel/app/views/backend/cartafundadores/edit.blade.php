@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto - A Escola
        </h2>  

		{{ Form::open( array('route' => array('painel.carta-fundadores.update', $registro->id), 'files' => false, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

		    	<div class="form-group">
					<label for="inputTexto1">Texto 1</label>
					<textarea name="texto1" id="inputTexto1" class="form-control">{{$registro->texto1}}</textarea>
				</div>

				<div class="form-group">
					<label for="inputTexto2">Texto 2</label>
					<textarea name="texto2" id="inputTexto2" class="form-control">{{$registro->texto2}}</textarea>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.carta-fundadores.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop