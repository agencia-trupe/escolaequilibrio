@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto - Relatório e Resultados
        </h2>  

		{{ Form::open( array('route' => array('painel.relatorios-e-resultados.update', $registro->id), 'files' => false, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

		    	<div class="form-group">
					<label for="inputTextoRelatorios">Texto Relatórios</label>
					<textarea name="texto_relatorio" id="inputTextoRelatorios" class="form-control">{{$registro->texto_relatorio}}</textarea>
				</div>

				<div class="form-group">
					<label for="inputTextoResultados">Texto Resultados</label>
					<textarea name="texto_resultados" id="inputTextoResultados" class="form-control">{{$registro->texto_resultados}}</textarea>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.relatorios-e-resultados.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop