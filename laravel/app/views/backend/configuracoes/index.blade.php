@section('conteudo')

    <div class="container add">

      	<h2>
        	Configurações
        </h2>

		{{ Form::open( array('route' => array('painel.configuracoes.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
					<label>Title</label>
					<input type="text" class="form-control" name="title"  value="{{ $registro->title }}">
                </div>

                <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control" name="description"  value="{{ $registro->description }}">
                </div>

                <div class="form-group">
                    <label>Keywords</label>
                    <input type="text" class="form-control" name="keywords" value="{{ $registro->keywords }}">
                </div>

                <hr>

                <div class="form-group">
                    <label>Código Google Analytics</label>
                    <input type="text" class="form-control" name="analytics" value="{{ $registro->analytics }}">
                </div>

                <div class="form-group">
                    <label>Código Google Adwords</label>
                    <input type="text" class="form-control" name="adwords" value="{{ $registro->adwords }}">
                </div>

                <div class="form-group">
                    <label>Código Facebook Pixel</label>
                    <input type="text" class="form-control" name="facebook_pixel" value="{{ $registro->facebook_pixel }}">
                </div>

                <hr>

                <div class="form-group">
		    		@if($registro->imagem_de_compartilhamento)
		    			<img src="{{ asset('assets/images/share/'.$registro->imagem_de_compartilhamento) }}" style="display:block;max-width:100%;margin-bottom:10px">
		    		@endif
					<label>Imagem de Compartilhamento</label>
					<input type="file" class="form-control" name="imagem_de_compartilhamento">
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>
			</div>
		</form>
    </div>

@stop
