@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto - Proposta Pwdagógica
        </h2>  

		{{ Form::open( array('route' => array('painel.proposta-pedagogica.update', $registro->id), 'files' => false, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

		    	<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" id="inputTexto" class="form-control">{{$registro->texto}}</textarea>
				</div>

				<div class="form-group">
					<label for="inputDestaque1">Destaque 1</label>
					<textarea name="destaque1" id="inputDestaque1" class="form-control">{{$registro->destaque1}}</textarea>
				</div>

				<div class="form-group">
					<label for="inputDestaque2">Destaque 2</label>
					<textarea name="destaque2" id="inputDestaque2" class="form-control">{{$registro->destaque2}}</textarea>
				</div>

				<div class="form-group">
					<label for="inputDestaque3">Destaque 3</label>
					<textarea name="destaque3" id="inputDestaque3" class="form-control">{{$registro->destaque3}}</textarea>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.proposta-pedagogica.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop