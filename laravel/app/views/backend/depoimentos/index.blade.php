@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
       <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if($errors->any())
		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	@endif

	<h2>
  		Depoimentos <a href="{{ URL::route('painel.depoimentos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus-sign"></span>  Adicionar Depoimento</a>
	</h2>

	<table class="table table-striped table-bordered table-hover table-sortable" data-tabela="depoimentos">
        
        <thead>
    		<tr>
            	<th>Ordenar</th>
            	<th>Imagem</th>
            	<th>Legenda</th>
      			<th><i class="icon-cog"></i></th>
    		</tr>
  		</thead>

  		<tbody>
  		@if(sizeof($registros) > 0)
	    	@foreach ($registros as $registro)

	        	<tr class="tr-row" id="row_{{ $registro->id }}">
	              	<td class="move-actions"><a href="#" class="btn btn-info btn-move btn-small">mover</a></td>
	              	<td><img src="{{ asset('assets/images/depoimentos/thumbs/'.$registro->imagem) }}"></td>
	              	<td>{{ $registro->legenda }}</td>
	          		<td class="crud-actions">
	            		<a href="{{ URL::route('painel.depoimentos.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>

	            	   {{ Form::open(array('route' => array('painel.depoimentos.destroy', $registro->id), 'method' => 'delete')) }}
	                        <button type="submit" class="btn btn-danger btn-sm btn-delete">excluir</button>
	                   {{ Form::close() }}
	          		</td>
	        	</tr>

	    	@endforeach
	    @else
            <tr>
              	<td colspan="4">
              	<h3>Nenhum Registro Encontrado</h3>
            	</td>
            </tr>
        @endif
  		</tbody>

    </table>
    
</div>

@stop