@section('conteudo')

    <div class="container add">

      	<h2>
        	Alterar Depoimento
        </h2>  

		{{ Form::open( array('route' => array('painel.depoimentos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

		    	<div class="form-group">
		    		@if($registro->imagem)
		    			<img src="{{ asset('assets/images/depoimentos/thumbs/'.$registro->imagem) }}"><br>
		    			<a href="{{ asset('assets/images/depoimentos/'.$registro->imagem) }}" target="_blank" title="Ver tamanho original">Ver tamanho original</a><br><br>
		    		@endif
					<label for="inputImagem">Imagem</label>
					<input type="file" class="form-control" id="inputImagem" name="imagem">
				</div>

				<div class="form-group">
					<label for="inputLegenda">Legenda</label>
					<input type="text" class="form-control" id="inputLegenda" name="legenda" value="{{ $registro->legenda }}">
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.depoimentos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop