<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="shortcut icon" href="favicon-jandaia.ico">

    <title>Escola Equilíbrio - Painel Administrativo</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />
    <meta property="og:title" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="{{ asset('assets/images/layout/'); }}"/>
    <meta property="og:url" content="{{ Request::url() }}"/>
    <meta property="og:description" content=""/>

  	<meta property="fb:admins" content=""/>
  	<meta property="fb:app_id" content=""/>

	<base href="{{ route('painel.home') }}/">
	<script>var BASE = "{{ route('painel.home') }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/bootstrap/dist/css/bootstrap.min',
		'css/painel/base'
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('js/vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('js/vendor/modernizr/modernizr'))?>
	@endif

</head>
	<body @if(Route::currentRouteName() == 'painel.login') class="body-painel-login" @else class="body-painel" @endif >

		@if(Route::currentRouteName() != 'painel.login')

			<nav class="navbar navbar-default">
				<div class="navbar-inner">
					<a href="{{URL::route('painel.home')}}" title="Página Inicial" class="navbar-brand">Escola Equilíbrio</a>

					<ul class="nav navbar-nav">

						<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.home')}}" title="Início">Início</a>
						</li>

						<li @if(str_is('*como-escolher*', Route::currentRouteName())) class="active" @endif >
							<a href="{{ URL::route('painel.como-escolher.index') }}" title="Como Escolher uma Escola">Como Escolher uma Escola</a>
						</li>

						<li class="dropdown @if(str_is('*carta-fundadores*', Route::currentRouteName()) || str_is('*a-escola*', Route::currentRouteName())) active @endif">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">A Escola <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ URL::route('painel.a-escola.index') }}" title="A Escola - Texto">Texto</a></li>
								<li><a href="{{ URL::route('painel.carta-fundadores.index') }}" title="A Escola - Carta dos Fundadores">Carta dos Fundadores</a></li>
							</ul>
						</li>

						<li @if(str_is('*proposta-pedagogica*', Route::currentRouteName())) class="active" @endif >
							<a href="{{ URL::route('painel.proposta-pedagogica.index') }}" title="Proposta Pedagógica">Proposta Pedagógica</a>
						</li>

						<li class="dropdown @if(str_is('*estrutura*', Route::currentRouteName())) active @endif">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Estrutura <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ URL::route('painel.estrutura.index') }}" title="Estrutura - Texto">Texto</a></li>
								<li><a href="{{ URL::route('painel.estrutura-imagens.index') }}" title="Estrutura - Imagens">Imagens</a></li>
							</ul>
						</li>

						<li @if(str_is('*relatorios-e-resultados*', Route::currentRouteName())) class="active" @endif >
							<a href="{{ URL::route('painel.relatorios-e-resultados.index') }}" title="Relatórios e resultados">Relatórios e resultados</a>
						</li>

						<li @if(str_is('*depoimentos*', Route::currentRouteName())) class="active" @endif >
							<a href="{{ URL::route('painel.depoimentos.index') }}" title="Depoimentos">Depoimentos</a>
						</li>

						<li @if(str_is('*atendimento*', Route::currentRouteName())) class="active" @endif >
							<a href="{{ URL::route('painel.atendimento.index') }}" title="Atendimento">Atendimento</a>
						</li>

						<li @if(str_is('*aulas-especificas*', Route::currentRouteName())) class="active" @endif >
							<a href="{{ URL::route('painel.aulas-especificas.index') }}" title="Aulas Específicas">Aulas Específicas</a>
						</li>

						<li @if(str_is('*contato*', Route::currentRouteName())) class="active" @endif >
							<a href="{{ URL::route('painel.contato.index') }}" title="Contato">Contato</a>
						</li>

						<li class="dropdown @if(str_is('*usuarios*', Route::currentRouteName()) || str_is('*configuracoes*', Route::currentRouteName()) || str_is('*seo*', Route::currentRouteName())) active @endif">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.configuracoes.index')}}" title="Configurações">Configurações</a></li>
								<li><a href="{{URL::route('painel.seo.index')}}" title="SEO">SEO</a></li>
								<li class="divider"></li>
								<li><a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários</a></li>
								<li><a href="{{URL::route('painel.off')}}" title="Logout">Logout</a></li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>

		@endif

		<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>

		<footer>
			<div class="container">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		@if(Route::currentRouteName() == 'painel.login')
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/login'
			))?>
		@else
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/painel'
			))?>
		@endif

	</body>
</html>
