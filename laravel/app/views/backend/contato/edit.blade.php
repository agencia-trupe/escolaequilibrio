@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Contato
        </h2>

		{{ Form::open( array('route' => array('painel.contato.update', $registro->id), 'files' => false, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone"  value="{{ $registro->telefone }}" required>
				</div>

				<div class="form-group">
					<label for="inputEndereco">Endereço</label>
					<textarea name="endereco" id="inputEndereco" class="form-control">{{$registro->endereco}}</textarea>
				</div>

				<div class="form-group">
					<label for="inputDetalhes">Detalhes do Endereço</label>
					<input type="text" class="form-control" id="inputDetalhes" name="endereco_detalhes"  value="{{ $registro->endereco_detalhes }}">
				</div>

				<div class="form-group">
					<label for="inputEnderecoRodape">Endereço a ser exibido no Rodapé</label>
					<textarea name="endereco_rodape" id="inputEnderecoRodape" class="form-control">{{$registro->endereco_rodape}}</textarea>
				</div>

				<div class="form-group">
					<label for="inputMaps">Código de Incorporação do Google Maps</label>
					<input type="text" class="form-control" id="inputMaps" name="google_maps"  value='{{ $registro->google_maps }}'>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>

@stop
