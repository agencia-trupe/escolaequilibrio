@section('conteudo')

    <div class="container add">

      	<h2>
        	<small>SEO /</small> {{ $registro->pagina }}
        </h2>

		{{ Form::open( array('route' => array('painel.seo.update', $registro->id), 'files' => false, 'method' => 'put') ) }}
			<div class="pad">
		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
					<label>Title</label>
					<input type="text" class="form-control" name="title"  value="{{ $registro->title }}">
                </div>

                <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control" name="description"  value="{{ $registro->description }}">
                </div>

                <div class="form-group">
                    <label>Keywords</label>
                    <input type="text" class="form-control" name="keywords" value="{{ $registro->keywords }}">
                </div>

                <button type="submit" title="Alterar" class="btn btn-success">Alterar</button>
                <a href="{{URL::route('painel.seo.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
			</div>
		</form>
    </div>

@stop
