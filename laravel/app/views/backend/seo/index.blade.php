@section('conteudo')

<div class="container">

	@if(Session::has('sucesso'))
       <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

	@if($errors->any())
		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	@endif

	<h2>
  		SEO
	</h2>

	<table class="table table-striped table-bordered table-hover">

        <thead>
    		<tr>
            	<th>Página</th>
      			<th><i class="icon-cog"></i></th>
    		</tr>
  		</thead>

  		<tbody>
  		@if(sizeof($registros) > 0)
	    	@foreach ($registros as $registro)
	        	<tr class="tr-row">
	              	<td>{{ $registro->pagina }}</td>
	          		<td class="crud-actions">
	            		<a href="{{ URL::route('painel.seo.edit', $registro->id ) }}" class="btn btn-primary btn-sm">editar</a>
	          		</td>
	        	</tr>
	    	@endforeach
	    @else
            <tr>
              	<td colspan="4">
              	<h3>Nenhum Registro Encontrado</h3>
            	</td>
            </tr>
        @endif
  		</tbody>

    </table>

</div>

@stop
