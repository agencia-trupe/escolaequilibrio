CREATE DATABASE  IF NOT EXISTS `equilibrio` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `equilibrio`;
-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: equilibrio
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.13.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `a_escola`
--

LOCK TABLES `a_escola` WRITE;
/*!40000 ALTER TABLE `a_escola` DISABLE KEYS */;
INSERT INTO `a_escola` VALUES (3,'<p>Texto</p>','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `a_escola` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `atendimento`
--

LOCK TABLES `atendimento` WRITE;
/*!40000 ALTER TABLE `atendimento` DISABLE KEYS */;
INSERT INTO `atendimento` VALUES (3,'<p>Texto</p>','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `atendimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `aulas_especificas`
--

LOCK TABLES `aulas_especificas` WRITE;
/*!40000 ALTER TABLE `aulas_especificas` DISABLE KEYS */;
INSERT INTO `aulas_especificas` VALUES (3,'<p>Texto</p>','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `aulas_especificas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `carta_fundadores`
--

LOCK TABLES `carta_fundadores` WRITE;
/*!40000 ALTER TABLE `carta_fundadores` DISABLE KEYS */;
INSERT INTO `carta_fundadores` VALUES (3,'<p>Texto 1</p>','<p>Texto 2</p>','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `carta_fundadores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `como_escolher`
--

LOCK TABLES `como_escolher` WRITE;
/*!40000 ALTER TABLE `como_escolher` DISABLE KEYS */;
INSERT INTO `como_escolher` VALUES (3,'<p>Texto</p>','0000-00-00 00:00:00','2014-02-27 21:37:11');
/*!40000 ALTER TABLE `como_escolher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (3,'(11) 3742-0521','<p>Rua Professor Hilário Veiga de Carvalho, 60<br>Morumbi - São Paulo - Capital</p>','Rua Professor Hilário Veiga de Carvalho, 60 &bull; Morumbi &bull; São Paulo - Capital','(Em frente ao Portal do Morumbi / ao lado do Outback e Pão de Açucar)','<iframe width=\'425\' height=\'350\' frameborder=\'0\' scrolling=\'no\' marginheight=\'0\' marginwidth=\'0\' src=\'https://maps.google.com.br/maps?q=Rua+Professor+Hil%C3%A1rio+Veiga+de+Carvalho,+60&amp;ie=UTF8&amp;hq=&amp;hnear=Rua+Professor+Hil%C3%A1rio+Veiga+de+Carvalho,+60+-+Vila+Suzana,+S%C3%A3o+Paulo,+05641-070&amp;gl=br&amp;t=m&amp;z=14&amp;ll=-23.619003,-46.737718&amp;output=embed\'></iframe><br /><small><a href=\'https://maps.google.com.br/maps?q=Rua+Professor+Hil%C3%A1rio+Veiga+de+Carvalho,+60&amp;ie=UTF8&amp;hq=&amp;hnear=Rua+Professor+Hil%C3%A1rio+Veiga+de+Carvalho,+60+-+Vila+Suzana,+S%C3%A3o+Paulo,+05641-070&amp;gl=br&amp;t=m&amp;z=14&amp;ll=-23.619003,-46.737718&amp;source=embed\' style=\'color:#0000FF;text-align:left\'>Exibir mapa ampliado</a></small>','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `depoimentos`
--

LOCK TABLES `depoimentos` WRITE;
/*!40000 ALTER TABLE `depoimentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `depoimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `estrutura`
--

LOCK TABLES `estrutura` WRITE;
/*!40000 ALTER TABLE `estrutura` DISABLE KEYS */;
INSERT INTO `estrutura` VALUES (3,'<p>Texto</p>','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `estrutura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `estrutura_imagens`
--

LOCK TABLES `estrutura_imagens` WRITE;
/*!40000 ALTER TABLE `estrutura_imagens` DISABLE KEYS */;
/*!40000 ALTER TABLE `estrutura_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_02_25_164955_create_como_escolher_table',1),('2014_02_25_165036_create_a_escola_table',1),('2014_02_25_165434_create_carta_fundadores_table',1),('2014_02_25_165630_create_proposta_pedagogica_table',1),('2014_02_25_171927_create_estrutura_table',1),('2014_02_25_172045_create_estrutura_imagens_table',1),('2014_02_25_172311_create_relatorio_e_resultados_table',1),('2014_02_25_172549_create_depoimentos_table',1),('2014_02_25_172804_create_atendimento_table',1),('2014_02_25_172928_create_aulas_especificas_table',1),('2014_02_25_173113_create_contato_table',1),('2014_02_25_173712_create_usuarios_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `proposta_pedagogica`
--

LOCK TABLES `proposta_pedagogica` WRITE;
/*!40000 ALTER TABLE `proposta_pedagogica` DISABLE KEYS */;
INSERT INTO `proposta_pedagogica` VALUES (3,'<p>Texto</p>','<p>destaque 1</p>','<p>destaque 2</p>','<p>destaque 3</p>','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `proposta_pedagogica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `relatorios_e_resultados`
--

LOCK TABLES `relatorios_e_resultados` WRITE;
/*!40000 ALTER TABLE `relatorios_e_resultados` DISABLE KEYS */;
INSERT INTO `relatorios_e_resultados` VALUES (2,'<p>Texto Relatórios</p>','<p>Texto Resultados</p>','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `relatorios_e_resultados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (9,'contato@trupe.net','trupe','$2y$10$zyhs4OTHdym8EdfUkf2TWeP1NYJZ5mkSXl3Yelv/MyD5/c4CKzOom','0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,'teste@teste','teste','$2y$10$wMIiwdLRLshDCsU//HeD0eHj6XBuxDxhvZjZQQg4UAnpUhMfTMrJu','2014-02-26 23:09:50','2014-02-27 17:41:32');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-02-27 17:38:03
